/**
 * This module uses readJSONFile function from load.js in the server
 * to find the definition of a word.
 * @author Maria Barba
 */

//load the load function
let load = require("../api/load.js");
//require http module
let http = require("http");
//create a global variable to store the parsed JSON file
let parsedJSONFile;
/**
 * loadJSONFile is an async function that calls the readJSONFile function
 * and checks whetehr the return value is null. If it is not null, it 
 * starts the server by calling the callToServer function
 */
async function loadJSONFile(){
  parsedJSONFile = await load.readJSONFile("./api/data.json");
  if(parsedJSONFile !== null){
    callToServer();
  }else{
    throw new Error("The JSON file containing definitions has not been parsed and it is null !");
  }
  
}
/**
 * callToServer starts the server and uses url search params to find the value that
 * the user provides for word. If the value in the provided word is not found or an error
 * occured : return a 404 error.
 */
function callToServer(){
  //Create the server
  let server = http.createServer();
  server.on("request", function (req, res) {
    //Prevent CORS error
    res.setHeader("Access-Control-Allow-Origin", "http://127.0.0.1:5501");
    try{
    //Create a URL object using the req and headers,host
      const url = new URL(req.url, `http://${req.headers.host}`);
      //if the serach parameter word has a value and the word has a definition in the dictionnary
      if(url.searchParams.has("word") && parsedJSONFile[ url.searchParams.get("word")] !== undefined){
        //get the word value
        let wordVal = url.searchParams.get("word");
        res.writeHead(200, {"Content-Type": "text/plain"});
        //Get the first definition for that word from data.json
        let returnedWord = parsedJSONFile[wordVal][0];
        //Write the returned word
        res.write(returnedWord);
           
      }else{
        //Write an error
        res.writeHead(404);
      }
      res.end(); 
    }catch(e){
      console.log(e);
    }
  });
  //Make server listen on port 3000
  server.listen(3000, function() {
    console.log("Server is listening on port 3000..."); 
  });
  
}


(function () {
  //load the JSON file in an IIFE function to avoid global scope clutter
  loadJSONFile();
})();

