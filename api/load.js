/**
 * readJSONFile is an async function that takes a filename and reads
 * the file with the provided name and then parses it to JSON.If there
 * is an error, console output the error and thow the error.
 * @param {String} filename 
 * @returns dictionnary object of the data.json file
 * @author Maria Barba
 */
const fs = require("fs/promises");
async function readJSONFile(filename){
  try {
    //Get the returned data
    let returnedData = await fs.readFile(filename);
    //return the parsed JSON
    return JSON.parse(returnedData);
  } catch (e) {
    //write the error to console
    console.error(e);
    //throw the error
    throw e;  
  }

}
//Export the function
module.exports.readJSONFile = readJSONFile;

