/**
 * This file uses jest to test the load module readJSONFile function
 * @uthor Maria Barba
 */
//require the load.js module
const load = require("../api/load.js");

/**
 * Test the function with a smaller json file containing informations about planets
 */
test("testcase1, parse JSON file with correct format", async () => {
  const file = "./__tests__/testcase1.json"
  const returnedJSON = await load.readJSONFile(file);
  expect(returnedJSON["Mercury"]).toBe(2440);
});

/**
 * Test the function for a txt file of incorrect format. Expect to throw an error
 * with the word Unexpected.
 */
test("testcase2 for readJSON, parse JSON file with incorrect format", async () => {
  const file = "./__tests__/testcase2.txt"
  await expect(load.readJSONFile(file)).rejects.toThrow("Unexpected");
});  
/**
 * Test the function for a JSON file that doesn't exist . Expect to throw an error
 * containing "no such file or directory".
 */
test("testcase3 for readJSON, parse JSON file that does not exist", async () => {
  const file = "./__tests__/testcase3.txt"
  await expect(load.readJSONFile(file)).rejects.toThrow("no such file or directory");
}); 

