"use strict";
/**
 * This scipt file creates a functional dictionnary website. It uses the api built in
 * the api.js module and makes requests to this API to get the definition of a word.
 * @author Maria Barba
 */
// Globals stores all the global values
let globals = {};
//Get a handle on the form
globals.form = document.querySelector("#form");
//Get a handle on the definitions paragraph
globals.defPara = document.querySelector("#definitionContainer");
//Get a handle on the error paragraph
globals.errorPara = document.querySelector("#errorContainer");
//get a handle on the api url without search params
globals.apiDict = "http://localhost:3000?";
/** Prevent form submisssion and add an event listener on submit
 * that calls the getDefinition function with the provided word
 * by the user.
 */
globals.form.addEventListener("submit", function(event){
  event.preventDefault();
  globals.inputValue = document.querySelector("#searchWord").value;
  getDefinition(globals.inputValue);
});
 
/**
 * getDefinition gets the definition of word by using the built API in api.js
 * and makes requests to this api.
 * @param {String} word is the word we are looking up the definition of
 */
async function getDefinition(word) {
  //Set up the search params
  let params = {word: word};
  let searchParams = new URLSearchParams(params);
  //Create the url we will use to look up the definition of the word
  let url = globals.apiDict + searchParams.toString();
  try {
    //fetch the response
    let response = await fetch(url);
    let content;
    if (response.ok) {
      //If there is no error,get the text form of the response
      content = await response.text();
    } else {
      //Otherwise throw an error
      throw new Error("Status code: " + response.status);
    }
    //display the definition
    displayDefinition(content);
  } catch(e) {
    //display the error
    displayError(e);
  }
}
  
/**
  * Provided with the definition, put it in the text value of 
  * the quotes paragraph.
  * @param definition 
  */
function displayDefinition(definition) {
  //log the definition
  console.log("The definition is : " + definition);
  //hide errors paragraph
  globals.errorPara.style.display = "none";
  //ddisplay the definition paragaraph
  globals.defPara.style.display = "flex";
  //set the content of definition paragraph 
  globals.defPara.textContent = `" ${definition} "`;

}
/**
  * Provided with the error status, display error
  * paragraph and adjust the text inside to 
  * the correct error message. Don't display the quotes paragraph.
  * @param {String with error status} message 
  */
function displayError(message) {
  //log the message
  console.log("The error message  is : " + message);
  //Display the error paragraph
  globals.errorPara.style.display = "flex";
  //Put error message inside the error paragraph
  globals.errorPara.textContent = "Definition not found, please try again";
  //Do not display the definition paragraph if there is an error
  globals.defPara.style.display = "none";
 
}
 